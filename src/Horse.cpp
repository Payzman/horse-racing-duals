#include "./Horse.hpp"

Horse::Horse(int str) {
    this->strength = str;
}

bool Horse::operator<(Horse other) {
    return this->strength < other.strength;
}

int Horse::operator-(Horse other) {
    return std::abs(this->strength - other.strength);
}

int Horse::getStrength() {
    return this->strength;
}
