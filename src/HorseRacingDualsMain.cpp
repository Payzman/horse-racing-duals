#include "./HorseRacingDualsMain.hpp"

int main() {
    int N;
    std::cin >> N; std::cin.ignore();
    Dual dual;
    for (int i = 0; i < N; i++) {
        int Pi;
        std::cin >> Pi; std::cin.ignore();
        dual.addHorse(Pi);
    }
    int minDifference = dual.getMinimumDifference();
    std::cout << minDifference << std::endl;
}
