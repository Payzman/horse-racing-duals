#ifndef HORSE_H_
#define HORSE_H_

#include <cstdlib>

class Horse {
    private:
        int strength;
    public:
        Horse(int);
        bool operator<(Horse other);
        int operator-(Horse other);
        int getStrength();
};

#endif // HORSE_H_
