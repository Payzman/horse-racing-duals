#include "./Dual.hpp"

void Dual::addHorse(Horse horse) {
    this->horses.push_back(horse);
}

int Dual::getMinimumDifference() {
    std::sort(horses.begin(), horses.end());
    std::vector<int> strengthDifference;
    for ( unsigned long i = 0; i < horses.size() - 1; i++ ) {
        strengthDifference.push_back( horses[i] - horses[i + 1] );
    }
    return *std::min_element(strengthDifference.begin(), strengthDifference.end());
}
