#ifndef DUAL_H_
#define DUAL_H_

#include <vector>
#include <algorithm>

#include "./Horse.hpp"

class Dual {
    private:
        std::vector<Horse> horses;
    public:
        void addHorse(Horse);
        int getMinimumDifference();
};

#endif // DUAL_H_
