#include "./HorseRacingDualsTest.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"

TEST(horses, sort) {
    std::vector<int> strengths = {2, 4, 1};
    std::vector<Horse> horses;
    for (int &strength : strengths ) {
        Horse horse = Horse(strength);
        horses.push_back(horse);
    }
    std::sort(horses.begin(), horses.end());
    ASSERT_EQ(horses.at(0).getStrength(), 1);
    ASSERT_EQ(horses.at(1).getStrength(), 2);
    ASSERT_EQ(horses.at(2).getStrength(), 4);
}

TEST(horses, strength_difference) {
    Horse A(2);
    Horse B(5);
    ASSERT_EQ(A - B, 3);
}

TEST(horses, closest_strength) {
    Dual dual;
    dual.addHorse(Horse(3));
    dual.addHorse(Horse(5));
    dual.addHorse(Horse(1));
    int minDifference = dual.getMinimumDifference();
    ASSERT_EQ(minDifference, 2);
}

#pragma clang diagnostic pop
